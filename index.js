require('dotenv').config();
const PORT = process.env.PORT ? process.env.PORT : 56201;

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const dayjs = require('dayjs');
const isLeapYear = require('dayjs/plugin/isLeapYear');
dayjs.extend(isLeapYear);
const server = express();
server.use(bodyParser.text());
server.use(morgan('combined'));

function processClientError(res, errMessage = 'Error occurred when processing request') {
    res.status(400).send(errMessage);
}

const getSquare = (number) => {
    return number * number;
}

const getReverse = (text) => {
    return (!text || Object.keys(text).length === 0) ? '' : text.split('').reverse().join('');
}

const getWeekday = (date) => {
    return date.format('dddd');
}

const isYearOfDateLeap = (date) => {
    return date.isLeapYear();
}

const getDifferenceBetweenDateAndCurrentDate = (date) => {
    const dateTime = date.toDate().getTime();
    const currentDateTime = new Date(new Date().toDateString()).getTime();
    const millis = Math.abs(dateTime - currentDateTime);
    return Math.round(millis / 86400000);
}

server.post('/square', function(req, res){
    const number = Number(req.body);
    if (!number && number !== 0) {
        processClientError(res);
        return;
    }
    const square = getSquare(number);
    res.status(200).json({number, square});
});

server.post('/reverse', function(req, res){
    const text = req.body;
    const reverse = getReverse(text);
    res.status(200).send(reverse);
});

server.get('/date/:year/:month/:day', function(req, res){
    const year = req.params.year;
    const month = req.params.month;
    const day = req.params.day;
    if (!year || !month || !day ||
        year.length !== 4 || (month.length !== 2 && month.length !== 1) || (day.length !== 2 && day.length !== 1) ||
        Number(month) > 12 || Number(day) > 31) {
        processClientError(res);
        return;
    }
    const date = dayjs(year + '-' + month + '-' + day);
    if (!date.isValid()) {
        processClientError(res);
        return;
    }
    const weekDay = getWeekday(date);
    const isLeapYear = isYearOfDateLeap(date);
    const difference = getDifferenceBetweenDateAndCurrentDate(date);
    res.status(200).json({weekDay, isLeapYear, difference});
});

server.listen(PORT, function () {
    console.log('Server is running on port ' + PORT)
});
